package net.tncy.ale.bookmarket.data;

import net.tncy.ale.validation.constraints.ISBN;

public class Book {
	@ISBN
	private String isbn;

	public Book(String isbn){
		this.isbn = isbn;
	}
}